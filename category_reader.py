# -*- coding: utf-8 -*-
"""
Created on Sun Oct 11 18:57:59 2020

@author: Ale
"""

import random
from utils import randkey



def category_splitter(category_element):
    
    subelements_list = []
    
    # The dot is the separation character; adding an extra one at the end
    # prevents the last word from hanging
    category_element_w_dot = category_element + '.'
    
    in_dollar = False
    current_word = ""
    
    for char in category_element_w_dot:
        
        assert char != " "
        
        if char == '.':
            assert current_word != ""
            if not in_dollar:
                subelements_list.append(current_word)
                current_word = ""
            else:
                current_word += '.'
        elif char == '$':
            in_dollar = not in_dollar
            current_word += char
        else:
            current_word += char
            
    return subelements_list 


def process_category(category_element, data, categories, segment_mappings):
    
    category_element_list = category_splitter(category_element)
    
    category_name = category_element_list[0]
    category_body = category_element_list[1:]
    
    labels = []
    acceptable_category_members = []
    
    if len(category_element_list) > 1:
        
        for category_label in category_body:
            if category_label[0] == category_label[-1] == '$': # Parameter case
                assert '.' in category_label
                [e0, e1] = category_label[1:-1].split('.')
                if e0 in data:
                    value = data[e0][e1]
                else:
                    assert e0 in segment_mappings
                    mapped_parameter = segment_mappings[e0][0][1:-1]
                    assert mapped_parameter in data
                    value = data[mapped_parameter][e1]
                             
                labels.append(value)
            else:   # Actual label name
                labels.append(category_label)
            
        
        
        for category_member in categories[category_name]:
            if labels == [] or set(labels).issubset(set(categories[category_name][category_member])):
                acceptable_category_members.append(category_member)
        
        if len(acceptable_category_members) == 0:
            return "@"
        elif len(acceptable_category_members) == 1:
            return acceptable_category_members[0]
        else:        
            chosen_category_member = acceptable_category_members[random.randint(0,len(acceptable_category_members)-1)]    
        
        return chosen_category_member
    
    else:
        return randkey(categories[category_name])
        
# DEPRECATED
def get_category_element(category_object, categories):
        
    category_element = ""
    
    category_list = categories[category_object]
    
    if category_list != []:
        category_element = category_list[random.randint(0,len(category_list)-1)]
    
    return category_element
   