# -*- coding: utf-8 -*-
"""
Created on Sun Aug 30 11:37:07 2020

@author: Ale
"""

import random
import utils

VERBPATH = r".\data\complete_verbs_forms.txt"
TENSEPATH = r".\data\verbs_tenses.txt"
ACTIONPATH = r".\data\actions.txt"
COMPFILE = r".\data\components.txt"
REQUIREMENTS = ["present", "past_simple", "perfect", "continuous"]

def linearize_verbs_file(filepath):
    
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    usable_lines = []
    for line in lines:
        clean_line = line.strip()
        
        if len(clean_line) > 0 and clean_line[0] != "#":
            usable_lines.append(clean_line)
    
    single_line_file = ""
    for line in usable_lines:
        single_line_file += str(line) + " "
    
    return single_line_file.strip()   

def parse_verb_language(single_line_file):
    
    verb_struct = {}
    words_list = []
    
    single_line_file = single_line_file.strip() + " "
    
    expecting_verb_label = True
    expecting_separator = False
    expecting_content = False
    in_verb_label = False
    in_content = False
    in_quote = False
    no_separator = False
    parentheses_content = False
    parentheses_count = 0
    
    last_label = ""
    current_word = ""
    
   
    for char in single_line_file:
        
        
        if in_quote and char in [' ', '{', '}', '$', '.', '\'']:
            current_word += char
            continue
                
        if char == " ":
            if in_verb_label:
                in_verb_label = False
                if no_separator:
                    words_list.append(current_word)
                    current_word = ""
                    in_verb_label = True
                else:
                    expecting_separator = True
                    last_label = current_word
                    if words_list == []:
                        words_list.append(current_word)
                    current_word = ""
            elif in_content:
                if not parentheses_content:
                    in_content = False
                    expecting_verb_label = True
                    verb_struct[last_label] = current_word.strip()
                    current_word = ""
                else:                    
                    current_word += char
        elif char == "{":
            assert not expecting_verb_label
            parentheses_count += 1
            if parentheses_count == 1 and expecting_content:
                expecting_content = False
                in_content = True
                parentheses_content = True
            elif in_content:
                current_word += char
        elif char == "}":
            parentheses_count -= 1
            assert parentheses_count >= 0
            if parentheses_count == 0:
                assert in_content
                in_content = False
                expecting_verb_label = True
                verb_struct[last_label] = current_word.strip()
                current_word = ""                
            else:
                current_word += char
        elif char == ":":
            words_list = []
            if expecting_separator:
                expecting_separator = False
                expecting_content = True
                current_word = ""
            else:
                current_word += char
        elif char == '"':
            in_quote = not in_quote
            current_word += char
        else:
            if expecting_verb_label:
                current_word += char
                expecting_verb_label = False
                in_verb_label = True
            elif expecting_separator:
                expecting_separator = False
                no_separator = True
                in_verb_label = True
                current_word += char
            elif expecting_content:
                current_word += char
                expecting_content = False
                in_content = True
                parentheses_content = False
            else:   #elif in_verb_label or in_content:
                current_word += char
                
    if current_word != "":
        verb_struct[last_label] = current_word
                
        
    if len(words_list) > 0:
        return words_list
    else:   
        return verb_struct

def parse_verbal_struct(verbs_struct):
    
    segmented_verb_struct = {}  
    
    for verb in verbs_struct:
        
        verb_cont = verbs_struct[verb]
        segmented_verb_struct[verb] = parse_verb_language(verb_cont)
    
    
    return segmented_verb_struct

def validate_single_verbs(verbs_struct, requirements):
    
    for verb in verbs_struct:
        verb_fields = list(verbs_struct[verb].keys())
        for req in requirements:
            if req not in verb_fields:
                return False
        
    return True

def segment_verbs(verbs_struct):
    
    segmented_verbs_struct = {}
        
    for verb in verbs_struct:
        
        verb_content = verbs_struct[verb]
        segmented_verbs_struct[verb] = {}
        
        for field in verb_content:
            
            field_content = verb_content[field]
            current_word = ""
            words_collection = []
                        
            for char in field_content:
                if char == " ":
                    words_collection.append(current_word)
                    current_word = ""
                else:
                    current_word += char
            
            if current_word != "":
                words_collection.append(current_word)
            
            if len(words_collection) > 1:
                segmented_verbs_struct[verb][field] = words_collection
            else:
                segmented_verbs_struct[verb][field] = words_collection[0]           
    
    return segmented_verbs_struct

def format_verbs(verbs_struct):
    
    for verb in verbs_struct:
        
        verbs_fields = verbs_struct[verb]
        
        for field in verbs_fields:
            
            if field == "past_simple":
                assert (type(verbs_fields[field]) == list and len(verbs_fields[field]) == 6) or type(verbs_fields[field] == str)
                
                if type(verbs_fields[field]) == str:
                    verbs_struct[verb][field] = [verbs_fields[field] for i in range(6)]
    
    
    return


def generate_verbs_structure(verbpath, requirements):
    
    slf = linearize_verbs_file(verbpath)
    cv = parse_verb_language(slf)
    svs = parse_verbal_struct(cv)
    b_v = validate_single_verbs(svs, requirements)
    
    assert b_v
    sev = segment_verbs(svs)
    format_verbs(sev)
    
    return sev

def generate_tense_structure(tensepath):    

    lt = linearize_verbs_file(tensepath)
    pt = parse_verb_language(lt)
    spt = parse_verbal_struct(pt)
    
    pst = spt 
    
    for s in spt:
        
        pst[s]["parameters"] = spt[s]["parameters"].split()
        
        s_struc = spt[s]["structure"].strip() + " "
        tense_struct_list = []
        in_word = False
        in_quotes = False
        current_word = ""
        
        for char in s_struc:
            
            if char == " ":
                if in_word:
                    if in_quotes:
                        current_word += char
                    else:
                        in_word = False
                        current_word = current_word.strip()
                        
                        if current_word[0] == current_word[-1] == '$':
                            assert current_word[1:-1] in pst[s]["parameters"]
                            
                        tense_struct_list.append(current_word)                        
                        current_word = ""
            elif char == '"':
                in_word = True
                in_quotes = not in_quotes
                current_word += char                    
            else:
                in_word = True
                current_word += char
        
        pst[s]["structure"] = tense_struct_list     
        
        
    return pst

def generate_action_structure(actionpath):

    linear_action_file = linearize_verbs_file(actionpath)
    parsed_action_file = parse_verb_language(linear_action_file)
    verbs_action_file = parse_verbal_struct(parsed_action_file)
    
    action_structure = {}
    
    for verb in verbs_action_file:
        verb_content = verbs_action_file[verb]
        action_structure[verb] = {}
        
        for field in verb_content:
            single_verb = parse_verb_language(verb_content[field])
            action_structure[verb][field] = single_verb
            single_verb = parse_verb_language(single_verb['logics'])
            action_structure[verb][field]['logics'] = single_verb
        
    return action_structure

def compile_tense(verb, tense, args, verbs):
    
    tense_structure = tense["structure"]
    final_tense = []
    
    for elem in tense_structure:
        
        clean_elem = elem.strip()
        if clean_elem [0] == clean_elem[-1] == "$":
            clean_elem = clean_elem[1:-1]
            
            if clean_elem in args:                
                arg = args[clean_elem]
                
                if type(arg) == list:                
                    arg_string = ""
                    
                    for a in arg:
                        arg_string += str(a) + " "
                    
                    arg_string = arg_string.strip()
                    final_tense.append(arg_string)
                else:
                    if arg != "":
                        final_tense.append(arg)
                
        elif clean_elem[0] == "<" and clean_elem[-1] == ">":
            clean_elem = clean_elem[1:-1]
            [verb_elem, form] = [x.strip() for x in clean_elem.split(".")]
            
            if verb_elem == "verb":
                verb_label = verb
            else:
                verb_label = verb_elem
            
            verb_form = verbs[verb_label][form]
            
            if type(verb_form) == str:
                final_tense.append(verb_form)
            elif type(verb_form) == list and len(verb_form) == 6:
                final_tense.append(verb_form[args["person"]].strip())
            else:
                return None
        else:
            return None        
                    
            
    
    return final_tense

def compile_action(action, args):
    
    # args[i] = (c_X, word)
    
    verb_idx = -1
    action_list = []
    
    for arg in args:
        
        if len(arg) > 1:
            (label, word) = arg
            
            if not label in action['logics'] or action['logics'][label] == "":
                action_list.append(word)
            else:
                word_to_add = action['logics'][label][1:-1] + " " + word
                action_list.append(word_to_add)
        elif len(arg) == 1:
            verb_idx = len(action_list)
        else:
            return -1
            
    return verb_idx, action_list

def recursive_file_parsing(linear_file):
    
    parsed_linear_file = parse_verb_language(linear_file)
    
    
    
    if type(parsed_linear_file) == dict:
        new_struct = {}
        
        for field in parsed_linear_file:
            body = parsed_linear_file[field]
            parsed_body = recursive_file_parsing(body)
            new_struct[field] = parsed_body
        
        return new_struct
    else:
        return parsed_linear_file            
            
    return

def generate_components_structure(filepath):
    
    linear_component_file = linearize_verbs_file(filepath)
    parsed_component_file = parse_verb_language(linear_component_file)
    processed_component_file = parse_verbal_struct(parsed_component_file)
            
    for component in processed_component_file:
        
        component_body = processed_component_file[component]
                
        if "type" not in component_body:
            processed_component_file[component]["type"] = ""
        
        if "requirements" in component_body:
            parsed_requirements = parse_verb_language(component_body["requirements"])
            processed_component_file[component]["requirements"] = parsed_requirements
        else:
            processed_component_file[component]["requirements"] = {}
        
        if "parameters" in component_body:
            component_body["parameters"] = component_body["parameters"].split() 
        
        if "structure" in component_body:
            processed_structure = parse_verb_language(component_body["structure"])
            structure_string = ""
            for s in processed_structure:
                structure_string += s + " "
            processed_component_file[component]["structure"] = parse_component_structure(structure_string.strip())         
    
    
    return processed_component_file

def parse_component_structure(comp_struct):
    
    
    comp_struct += " "
    
    in_brackets = False
    in_quotes = False
    in_dollar = False
    current_word = ""
    words_list = []
    
    for char in comp_struct:
        
        if char == "<":
            in_brackets = True
            current_word += char
        elif char == ">":
            in_brackets = False
            current_word += char
        elif char == "$":
            in_dollar = not in_dollar
            current_word += char
        elif char == '"':
            in_quotes = not in_quotes
            current_word += char
        elif char == " ":
            if not (in_brackets or in_quotes or in_dollar):
                if current_word != "":
                    words_list.append(current_word)
                    current_word = ""
            else:
                current_word += char
        else:
            current_word += char
        
    
    final_word_list = parse_component_actions(words_list)        
        
    return final_word_list


def parse_component_action_args(args):
        
    args = args.strip()
    
    in_label = False
    in_argument = False
    label = ""
    argument = ""
    current_word = ""
    
    # List of the arguments of the action
    args_list = []
    # An argument may be madeup of multiple elements
    multiple_elements_list = []
    
    current_label_struc = {}
    
    for char in args:
        
        
        if char == "=":
            label = current_word            
            current_label_struc[label] = []
            current_word = ""
            in_label = False
        elif char == ",":
            argument = current_word
            multiple_elements_list.append(argument)
            current_label_struc[label].append(multiple_elements_list)
            args_list.append(current_label_struc)
            current_word = ""
            in_argument = False
            label = ""
            argument = ""
            current_label_struc = {}
        elif char == " ":
            if in_argument:
                multiple_elements_list.append(current_word)
                in_argument = False
                current_word = ""
        else:
            if label == "":                
                in_label = True
                current_word += char
                multiple_elements_list = []
            else:
                in_argument = True
                current_word += char
                    
    if current_word != "" and label != "":
        multiple_elements_list.append(current_word)
        current_label_struc[label].append(multiple_elements_list)
        args_list.append(current_label_struc)
    
    return args_list

def parse_component_actions(words_list):
    
    new_words_list = []
    
    for word in words_list:
        clean_word = word.strip()
        if clean_word[0] == "<":
            assert clean_word[-1] == ">"
            
                        
            clean_word = clean_word[1:-1].strip() + " "
            
            action_struc = {"action":"",
                            "tense":"",
                            "tense_pars":"",
                            "action_pars":""}
            
            current_word = ""
            in_dollar = False
            in_quote = False
            in_tense_def = False
            in_tense_pars = False
            in_action_pars = False
            
            for char in clean_word:
                
                if char == "(":
                    if current_word != "":
                        if action_struc["action"] == "":
                            action_struc["action"] = current_word
                        current_word = ""
                    if action_struc["tense"] == "":
                        in_tense_def = True
                    elif action_struc["tense_pars"] == "":
                        in_tense_pars = True
                    else:
                        in_action_pars = True
                elif char == ")":                    
                    if in_tense_def:
                        in_tense_def = False
                        action_struc["tense"] = current_word.strip()
                    elif in_tense_pars:
                        in_tense_pars = False
                        action_struc["tense_pars"] = parse_component_action_args(current_word)
                    elif in_action_pars:
                        in_action_pars = False
                        action_struc["action_pars"] = parse_component_action_args(current_word)
                    current_word = ""
                elif char == "$":
                    in_dollar = not in_dollar
                    current_word += char
                else:
                    current_word += char
            
            new_words_list.append(action_struc)
                    
            
        else:
            new_words_list.append(word)
    
    return new_words_list

# =============================================================================
# parameter: (label, [content])
# data: {sender:{X}, receiver:{Y}, ...}
# action: {verb1:{}, verb2:{}, ...}
# =============================================================================
def process_component_parameters(parameter, data, action):
        
    string_ret = ""
    
    param_head = parameter[0].strip()
    param_body = ""
    if len(parameter) > 1:
        param_body = parameter[1]
        
    if param_head == "subject":        
        # Determine subject pronoun        
        
        if len(param_body) == 1:            
            subject_param = param_body[0]
            aux = ""
            for word in subject_param:
                if word[0] == word[-1] == "$":
                    new_word = word[1:-1]
                    if "." in new_word:
                        [w0, w1] = new_word.split('.')
                        if w0 in data:
                            aux += str(data[w0][w1]) + " "
                            print(">>>", data[w0][w1])
                        else:
                            print("WORD NOT IN DATA")
                    else:
                        aux += str(new_word) + " "
                else:
                    aux += str(word) + " "
                    
            subject_param = aux.strip()   
            
            if subject_param == "sender":
                string_ret += "I"
            elif subject_param == "receiver":
                string_ret += "you"
            else:
                string_ret += str(subject_param)
                    
        else:
            string_ret += "MULTIPLE_SUBJECTS"
    elif param_head == 'verb':
        print("VERB")
    else:
        # FIRST VERB SELECTED, MOMENTARY FIX
        action_verb_head = list(action.keys())[0]
        action_verb_body = action[action_verb_head]
        action_logics = action_verb_body["logics"]
        
        if param_head in action_logics:
            param_preposition = action_logics[param_head]
            
            aux_list = []
            
            for element in param_body:
                element_string = ""
                for word in element:
                    if word[0] == word[-1] == '$':
                        new_word = word[1:-1]
                        if '.' in new_word:
                            [w0, w1] = new_word.split('.')
                            element_string += data[w0][w1] + " "
                aux_list.append(element_string)
            
            for element in aux_list:
                string_ret += str(element)            
            
        else:
            string_ret = str(parameter)
    
    return string_ret

def interpret_component(component, data, actions):
    
    elements_list = []
    
    print(component['structure'], '\n')
            
    for element in component["structure"]:
                
        if type(element) == str:
            if element[0] == element[-1] == '"':
                elements_list.append(element)
            elif element[0] == element[-1] == "$":
                parameter = element[1:-1]
                if "." in parameter:
                    [entity, field] = parameter.split(".")
                    elements_list.append(data[entity][field])
                else:
                    elements_list.append(data[entity]["id"])
            elif len(element) > 1 and element[0] == "\\":
                if element == "\\n":
                    elements_list.append('\n')
        else:
                        
            assert type(element) == dict
            
            
            e_action = element["action"]
            e_action_pars = element["action_pars"]
            e_tense = element["tense"]
            e_tense_pars = element["tense_pars"]
            
            
            processed_action_pars = []
            for par in e_action_pars:
                
                head = utils.nthkey(par) 
                body = par[head]
                                
                if head == "subject":
                    processed_action_pars.append((head, body,))
                    processed_action_pars.append(('verb',))
                else:
                    processed_action_pars.append((head, body,))

            for pap in processed_action_pars:
                pcp = process_component_parameters(pap, data, actions[e_action])
                print(">", pcp)
            
            elements_list.append(processed_action_pars)                      
    
    
    return elements_list

#==============================================================================

verb_structure = generate_verbs_structure(VERBPATH, REQUIREMENTS)
tense_structure = generate_tense_structure(TENSEPATH)
action_structure = generate_action_structure(ACTIONPATH)
component_structure = generate_components_structure(COMPFILE)

people = {
    "0000":{"id":"0000",
          "name":"John",
          "surname":"Smith",
          "gender":"male",
          "company":"Fictitious Inc.",
          "position":"PR"},    
    "0001":{"id":"0001",
            "name":"Bob",
          "surname":"Ross",
          "gender":"male",
          "company":"AWATO Corp.",
          "position":"Accountant"}
}

cc = component_structure["introduce_oneself"]
ag = {"sender":people["0000"], "receiver":people["0001"]}
ic = interpret_component(cc, ag, action_structure)


# =============================================================================
# print(ic)
# =============================================================================
