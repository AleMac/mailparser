# Request mails
comp_informal_greeting_0 : {

	parameters : {}
	
	structure : {
	
		0 : {
			type : string
			text : { "Hey " $receiver.name$ "," \n }
		}
		
		
	}

}

comp_informal_greeting_1 : {

	parameters : {}
	
	structure : {
	
		0 : {
			type : string
			text : { "Hi " $receiver.name$ "," \n }
		}
		
	}

}

comp_informal_greeting_2 : {

	parameters : {}
	
	structure : {
	
		0 : {
			type : string
			text : { "Hey," \n }
		}
		
	}

}

comp_request_work_0 : {
	
	parameters : {  }
	
	
	structure : {
		0 :{
		
			type : action
			
			name : act_need_0
			
			tense : present_simple
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : { "the" %workplace_info% }
				c_time_limit : { "tomorrow."}
			}
			
			order : { subject verb c_object c_time_limit}
			
		}
	}
}


comp_request_work_1 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { "could" }
			
		}
		
		1 :{
		
			type : action
			
			name : act_send_0
			
			tense : infinite
			
			verb_parameters : {}

			action_parameters : {
				subject : { $receiver$ }
				verb :{}
				c_object : { "the" %workplace_info% }
				c_indirect : { $sender$ }
			}			
			
			order : { subject verb c_object c_indirect }
			
		}
		
		2 : {
			type : string
			text : { "please?" }
		}
	}
}

comp_request_work_2 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { "could" }
			
		}
		
		1 :{
		
			type : action
			
			name : act_send_1
			
			tense : infinite
			
			verb_parameters : {}

			action_parameters : {
				subject : { $receiver$ }
				verb :{}
				c_object : { "the" %workplace_info% }
				c_indirect : { $sender$ }
			}			
			
			order : { subject verb c_indirect c_object }
			
		}
		
		2 : {
			type : string
			text : { "please?" }
		}
	}
}

comp_gratitude_informal : {
	
	parameters : {  }
	
	
	structure : {
		0 :{
		
			type : string
			text : { %informal_gratitude% }
			
		}
		
		1 : {
			type : string
			text : { "." }
		
		}
		
		2 :{
		
			type : string
			text : {  \n}
			
		}
	}
}

comp_informal_signature_0 : {
	
	parameters : {  }
	
	
	structure : {
		0 :{
		
			type : string
			text : { $sender.name$ }
			
		}
	}
}

comp_informal_signature_1 : {
	
	parameters : {  }
	
	
	structure : {
		0 :{
		
			type : string
			text : { $sender.name$ ", " $sender.position$ }
			
		}
	}
}

comp_signature_0 : {
	
	parameters : {  }
	
	
	structure : {
		0 :{
		
			type : string
			text : { $sender.name$ " " $sender.surname$ }
			
		}
	}
}

comp_urgency_request_0 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { "please" }
			
		}
		
		1 :{
		
			type : action
			
			name : act_send_0
			
			tense : infinite
			
			verb_parameters : {}

			action_parameters : {
				subject : { $receiver$ }
				verb :{}
				c_object : { "it" }
				c_indirect : { $sender$ %urgency_words% "."}
			}			
			
			order : { verb c_object c_indirect }
			
		}

	}
}

# Scam mail


comp_polite_opening_0 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { "Dear " $receiver.name$ "," \n }
			
		}
		
	}
}

comp_polite_opening_1 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { "Greetings," \n }
			
		}
		
	}
}

comp_polite_opening_2 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { "Esteemed " %gendered_titles.$receiver.gender$% " " $receiver.surname$ "," \n }
			
		}
		
	}
}

comp_identification_0 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { "My name is " $sender.name$ $sender.surname$ "." }
			
		}
		
	}
}

comp_identification_1 : {
	
	parameters : {  }	
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_be
			
			tense : present_simple
			
			verb_parameters : {}

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_nominal : { $sender.name$ $sender.surname$ "." }
			}			
			
			order : { subject verb c_nominal }
			
		}
		
	}
}

comp_relative_knowledge_0 : {
	
	parameters : { comp_rel }	
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_be
			
			tense : present_simple
			
			verb_parameters : {}

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_nominal : { "a " %present_and_past_relations% " of your" %relatives.$comp_rel.gender$% " " $comp_rel.name$ }
			}			
			
			order : { subject verb c_nominal }
			
		}
		
		1 : {
			type : string
			text : { "and" }
		}
		
		2 :{
		
			type : action
			
			name : act_know_0
			
			tense : present_perfect
			
			verb_parameters : {}

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : { "him" }
				c_time_duration : { "years." }
			}			
			
			order : { subject verb c_object c_time_duration }
			
		}
		
	}
}

comp_relative_knowledge_1 : {
	
	parameters : {  }	
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_be
			
			tense : past_perfect
			
			verb_parameters : {}

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_nominal : { "a " %present_relations% " of your" %relatives% " " $comp_rel.name$}
			}			
			
			order : { subject verb c_nominal }
			
		}
		
		1 : {
			type : string
			text : { "and" }
		}
		
		2 :{
		
			type : action
			
			name : act_know_0
			
			tense : present_perfect
			
			verb_parameters : {}

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : { "him" }
				c_time_duration : { "years." }
			}			
			
			order : { subject verb c_object c_time_duration }
			
		}
	}
}

comp_relative_knowledge_2 : {
	
	parameters : {  }	
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_be
			
			tense : past_simple
			
			verb_parameters : {}

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_nominal : { "a " %present_relations% " of your" %relatives% " " $comp_rel.name$ }
			}			
			
			order : { subject verb c_nominal }
			
		}
		
		1 : {
			type : string
			text : { "and" }
		}
		
		2 :{
		
			type : action
			
			name : act_know_0
			
			tense : present_perfect
			
			verb_parameters : {}

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : { "him" }
				c_time_duration : { "years." }
			}			
			
			order : { subject verb c_object c_time_duration }
			
		}
		
	}
}


comp_money_request_reason_0 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
		
			type : action
			
			name : act_fall_0
			
			tense : present_perfect
			
			verb_parameters : { p1 : "recently" }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_comp0 : { "hard times" }
			}			
			
			order : { subject verb c_comp0 }
			
		}
		
		1 : {
			type : string
			text : { "due to a " %recent_troubles% }
		}
		
		2 : {
			type : string
			text : { "and" }
		}
		3 : {
		
			type : action
			
			name : act_be
			
			tense : present_simple
			
			verb_parameters : { p1 : "desperately" }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_need : { "a small sum of money" }
			}			
			
			order : { subject verb c_need }
			
		}
		4 : {
			type : string
			text : { "." }
		}
		
	}
}

comp_money_request_proper_0 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { "Please " }
			
		}
	
		1 : {
		
			type : action
			
			name : act_send_1
			
			tense : infinite
			
			verb_parameters : { }

			action_parameters : {
				subject : { $receiver$ }
				verb :{}
				c_object : { "an e-mail" }
				c_indirect : { $sender$ }
			}			
			
			order : { verb c_indirect c_object }
			
		}
		
		2 : {
			type : string
			text : { "to" }
		}
			
		3 : {
		
			type : action
			
			name : act_discuss_0
			
			tense : infinite
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_theme : { "the money transfer" }
			}			
			
			order : { verb c_theme }
			
		}
		4: {
			type : string
			text : { "." }
		}
		5: {
			type : string
			text : { \n }
		}
		
	}
}

comp_money_request_proper_1 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { "Please " }
			
		}
	
		1 : {
		
			type : action
			
			name : act_send_1
			
			tense : infinite
			
			verb_parameters : { }

			action_parameters : {
				subject : { $receiver$ }
				verb :{}
				c_object : { "a mail" }
				c_indirect : { $sender$ }
			}			
			
			order : {  verb c_indirect c_object }
			
		}
		
		2 : {
			type : string
			text : { "so that" }
		}
			
		3 : {
		
			type : action
			
			name : act_know_0
			
			tense : infinite
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : { "you want to help me" }
			}			
			
			order : { subject verb c_object }
			
		}
		4: {
			type : string
			text : { "." }
		}
		5: {
			type : string
			text : { \n }
		}
		
	}
}

comp_seg_polite_blessing_0 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { %polite_blessings% ","}
			
		}
		5: {
			type : string
			text : { \n }
		}
		
	}
}

comp_formal_signature_0 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { $sender.name$ $sender.surname$}
			
		}
		
	}
}

comp_formal_signature_1 : {
	
	parameters : {  }	
	
	structure : {
	
		0 : {
			
			type : string
			text : { $sender.surname$ ", " $sender.name$}
			
		}
		
	}
}





#=================================================
# Test Portion
#=================================================



introduce_oneself : {
	
	parameters : {}

	structure : {
	
		0 :{
		
			type : action
			
			name : act_be
			
			tense : present_simple
			
			verb_parameters : {}

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_nominal : { $sender.name$ $sender.surname$ }
			}			
			
			order : { subject verb c_nominal }
			
		}
		
		1 : {
			
			type : string
			
			text : " and "
		
		}
		
		2 : {
			
			name : { act_work }
			
			type : { action }
			
			tense : { present_simple }
			
			verb_parameters : { }
			
			action_parameters : {
				subject : { $sender$ }
				verb : {}
				c_advantage : { $sender.company$ }
			}
			
			order : { subject verb c_advantage }
			
		}
		
		3 : {
			
			type : string
			
			text : "."
		
		}		
		
	}

}


greetings_0 : {

	parameters : { }

	structure : {
	
		0 :{
		
			type : string
			
			text : { "Dear Mr." $receiver.surname$ "," \n }
			
		}
		
	}

}

greetings_1 : {

	parameters : { }

	structure : {
	
		0 :{
		
			type : string
			
			text : { "Esteemed Mr." $receiver.surname$ "," \n }
			
		}
		
	}

}

ask_to_write : {

	parameters : { $writeto_person$ }
	
	structure : {
		
		0 : {
		
			type : string
			
			text : "Please, "
		}
		
		1 : {
			
			type : action
			
			name : act_write_1
			
			tense : { infinite }
			
			verb_parameters : {}
			
			action_parameters : {
				subject : $receiver$
				verb : {}
				c_indirect : { $writeto_person.name$ }
				c_theme : { "the" %project_details% "of" %project_names% }
			}
		
			order : { verb c_indirect c_theme }
		}
		
		2 : {
			type : string
			text : "."
		}
		
	}
	
}

goodbye_0: {
	
	parameters : {}
	
	structure: {
		0: {
			type : string
			text : { \n "Regards," \n $sender.name$ $sender.surname$ }
		}
	}
}

goodbye_1 : {
	
	parameters : {}
	
	structure: {
		0 : {
			type : string
			text : { \n "Best regards," \n $sender.name$ $sender.surname$ }
		}
	}
}
