# Introduction components, say you worked on a project
comp_clar_intro_0 : {

	parameters : { }
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_work_0
			
			tense : present_perfect_continuous
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_theme : {"one of our projects"}
			}
			
			order : { subject verb c_theme }
			
		}
		
		1 : {
			type : string
			text : { "and" }
		}
		
	}

}

comp_clar_intro_1 : {

	parameters : { }
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_write_0
			
			tense : present_perfect
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : {"this mail"}
				c_indirect : { $receiver$ }
			}
			
			order : { subject verb c_object c_indirect }
			
		}
		
	}

}

comp_clar_intro_2 : {

	parameters : { }
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_send_0
			
			tense : present_continuous
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : {"this mail"}
				c_indirect_1 : { $receiver$ }
			}
			
			order : { subject verb c_indirect_1 c_object }
			
		}
		
		1 : {
			type : string
			text : { "because" }
		}
		
	}

}

comp_clar_intro_3 : {

	parameters : { }
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_send_0
			
			tense : present_continuous
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : {"this mail"}
				c_indirect_1 : { $receiver$ }
			}
			
			order : { subject verb c_indirect_1 c_object }
			
		}
		
	}

}


# Express need for clarification
comp_clar_need_0 : {

	parameters : { }
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_have_0
			
			tense : present_simple
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
			}
			
			order : { subject verb }
			
		}
		
		1 :{
		
			type : action
			
			name : act_ask_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }

			action_parameters : {
				subject : { "NONE" }
				verb :{}
				c_indirect :{ $receiver$ }
				c_object : { %question.interrogative% }
				c_theme : { %mecella_projects.single_word% }
				
			}
			
			order : { verb c_indirect c_object c_theme }
			
		}
		
	}

}

comp_clar_need_1 : {

	parameters : { }
	
	structure : {
		
		1 :{
		
			type : action
			
			name : act_ask_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }

			action_parameters : {
				subject : { "NONE" }
				verb :{}
				c_indirect :{ $receiver$ }
				c_object : { %question.interrogative% }
				c_theme : { %mecella_projects.single_word% }
				
			}
			
			order : { verb c_indirect c_object c_theme }
			
		}
		
	}

}

comp_clar_need_2 : {

	parameters : { }
	
	structure : {
		
		1 :{
		
			type : action
			
			name : act_need_0
			
			tense : present_simple
			
			verb_parameters : {  }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : { %question.positive% }
				c_theme : { %mecella_projects.single_word% }
				
			}
			
			order : { subject verb c_object c_theme }
			
		}
		
	}

}

comp_clar_need_3 : {

	parameters : { }
	
	structure : {
		
		1 :{
		
			type : action
			
			name : act_like_0
			
			tense : infinite
			
			verb_parameters : { p0 : "would" }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : { %question.positive% }
				
			}
			
			order : { subject verb c_object }
			
		}
		
		2 : {
			type : string
			text : { %preposition_theme% " project " %mecella_projects.single_word% }
		}
		
	}

}

comp_clar_need_4 : {

	parameters : { }
	
	structure : {
		
		1 :{
		
			type : action
			
			name : act_request_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }

			action_parameters : {
				subject : { "NONE" }
				verb :{}
				c_object : { %question.positive% }
				
			}
			
			order : { verb c_object }
			
		}
		
		2 : {
			type : string
			text : { %preposition_theme% " project" %mecella_projects.single_word% }
		}
		
	}

}


# Actual question
comp_clar_quest_0 : {

	parameters : { }
	
	structure : {
	
		0 : {
			type : string
			text : { "in particular" \, }
		}
		
		1 :{
		
			type : action
			
			name : act_need_0
			
			tense : present_simple
			
			verb_parameters : {  }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				
			}
			
			order : { subject verb }
			
		}
		
		2 :{
		
			type : action
			
			name : act_know_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }

			action_parameters : {
				subject : { "NONE" }
				verb :{}
				c_object : { %info.crucial% }
				c_time_before : { "proceeding forward" }
				
			}
			
			order : { verb c_object c_time_before }
			
		}
		
		3 : {
			type : string
			text : { \, "as" }
		}
		
		4 :{
		
			type : action
			
			name : act_think_0
			
			tense : infinite
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object_1 : { %request_reason.crucial% }
				
			}
			
			order : { subject verb c_object_1 }
			
		}
		
	}

}

comp_clar_quest_1 : {

	parameters : { }
	
	structure : {
	
		0 : {
			type : string
			text : { "in particular" \, }
		}
		
		1 :{
		
			type : action
			
			name : act_like_0
			
			tense : infinite
			
			verb_parameters : { p0 : "would" }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				
			}
			
			order : { subject verb }
			
		}
		
		2 :{
		
			type : action
			
			name : act_explain_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }

			action_parameters : {
				subject : { $receiver$ }
				verb :{}
				c_object : { %info.crucial% }
				c_target : { $sender$ }
				
			}
			
			order : { subject verb c_target c_object }
			
		}
	
		3: {
			type : string
			text : { \, "since " %request_reason.crucial% }
		}
	
		4: {
			type : string
			text : { "and" }
		}
		
		5 :{
		
			type : action
			
			name : act_proceed_0
			
			tense : infinite
			
			verb_parameters : { p0 : "really can't" }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_time : { "right now" }
				
			}
			
			order : { subject verb c_time }
			
		}
	
		6 : {
			type : string
			text : { "without knowing it" }
		}
		
	}

}

comp_clar_quest_2 : {

	parameters : { }
	
	structure : {	
		
		1 :{
		
			type : action
			
			name : act_do_0
			
			tense : present_simple
			
			verb_parameters : { p1 : "not" }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : {"remember"}
			}
			
			order : { subject verb c_object}
			
		}
		
		2 : {
			type : string
			text : { "how" }
		}
		
		3 :{
		
			type : action
			
			name : act_intend_0
			
			tense : present_simple
			
			verb_parameters : { }
			
			person : 3

			action_parameters : {
				subject : { "we" }
				verb :{}
			}
			
			order : { subject verb }
			
		}
		
		4 :{
		
			type : action
			
			name : act_act_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }

			action_parameters : {
				subject : { "NONE" }
				verb :{}
				c_theme : {%info.crucial%}
			}
			
			order : { verb c_theme}
			
		}
		
		5 : {
			type : string
			text : { \, "could" }
		}
		
		6 :{
		
			type : action
			
			name : act_talk_0
			
			tense : infinite
			
			verb_parameters : {  }
			
			person : 3

			action_parameters : {
				subject : { "we" }
				verb :{}
				c_theme : {"it"}
			}
			
			order : { subject verb c_theme }
			
		}
		
		7 : {
			type : string
			text : { \?}
		}
		
		8 :{
		
			type : action
			
			name : act_think_0
			
			tense : present_simple
			
			verb_parameters : {  }
			
			action_parameters : {
				subject : { $sender$ }
				verb :{}
			}
			
			order : { subject verb }
			
		}
		
		9 :{
		
			type : action
			
			name : act_be_0
			
			tense : present_simple
			
			verb_parameters : {  }
			
			action_parameters : {
				subject : { "it" }
				verb :{}
				c_nominal : {"something important to have clear"}
			}
			
			order : { verb c_nominal }
			
		}

		
	}

}


# End components
comp_clar_end_0 : {

	parameters : { }
	
	structure : {
	
		0 : {
			type : string
			text : {"please" \, }
		}
		
		1 :{
		
			type : action
			
			name : act_let_0
			
			tense : present_simple
			
			verb_parameters : { }
			
			action_parameters : {
				subject : { $receiver$ }
				verb :{}
				c_object : { "me" }
				
			}
			
			order : { verb c_object }
			
		}
		
		2 :{
		
			type : action
			
			name : act_know_0
			
			tense : infinite
			
			verb_parameters : { }

			action_parameters : {
				subject : { "NONE" }
				verb :{}
				c_object : { "your answer" }
				c_time : { "you can" }
				
			}
			
			order : { verb c_object c_time}
			
		}
		
	}

}

comp_clar_end_1 : {

	parameters : { }
	
	structure : {
			
		1 :{
		
			type : action
			
			name : act_wait_0
			
			tense : future_simple
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_target : { "your response" }
				c_time_before : { "proceeding with the work" }
				
			}
			
			order : { subject verb c_target c_time_before }
			
		}
	}

}

comp_clar_end_2 : {

	parameters : { }
	
	structure : {
			
		1 :{
		
			type : action
			
			name : act_have_0
			
			tense : present_simple
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : { %words_0.question.plural% }
				c_theme_1 : { "the project" }
				
			}
			
			order : { subject verb c_object c_theme_1 }
			
		}
		
		2 : {
			type : string
			text : { \, "but" }
		}
			
		3 :{
		
			type : action
			
			name : act_be_0
			
			tense : past_simple
			
			verb_parameters : { }

			action_parameters : {
				subject : { "this" }
				verb :{}
				c_nominal : { "the most pressing one" }				
			}
			
			order : { subject verb c_nominal  }
			
		}		
		
		
		4 : {
			type : string
			text : { \. }
		}
			
		5 :{
		
			type : action
			
			name : act_have_0
			
			tense : infinite
			
			verb_parameters : { p0 : "should" }
			
			person : 3

			action_parameters : {
				subject : { "we" }
				verb :{}
				c_object : { "a sync" }				
			}
			
			order : { subject verb c_object }
			
		}
			
		6 :{
		
			type : action
			
			name : act_discuss_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }
			

			action_parameters : {
				subject : { "NONE" }
				verb :{}
				c_object : { "the rest" }				
			}
			
			order : {  verb c_object }
			
		}
		
	}

}