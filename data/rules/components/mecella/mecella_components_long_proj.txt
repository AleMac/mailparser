comp_project_long_progress_0 : {

	parameters : { proj coll }
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_continue_0
			
			tense : present_perfect
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
			}
			
			order : { subject verb }
			
		}
		
		1 : {
		
			type : action
			
			name : act_work_0
			
			tense : continuous_form
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_theme : { $proj.name$ }
			}
			
			order : { verb c_theme}
			
		}
		
		2 : {
			type : string
			text : { "and" }
		}
		
		3 : {
		
			type : action
			
			name : act_make_0
			
			tense : present_perfect
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : { "some good progress" }
			}
			
			order : { subject verb c_object }
			
		}
		
	}

}

comp_project_long_progress_1 : {

	parameters : { proj coll }
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_make_0
			
			tense : present_perfect_continuous
			
			verb_parameters : { }

			action_parameters : {
				subject : { "me" $coll.name$ }
				verb :{}
				c_object : { "decent progress" }
				c_theme_1 : { $proj.name$ }
				c_time : { "the last days" }
			}
			
			order : { subject verb c_object c_theme_1 c_time }
			
		}
		
		
		
	}

}

comp_project_long_progress_2 : {

	parameters : { proj coll }
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_continue_0
			
			tense : present_perfect
			
			verb_parameters : { }

			action_parameters : {
				subject : { "work" }
				verb :{}
				c_theme : { $proj.name$ }
			}
			
			order : { subject verb c_theme }
			
		}
		
		1 : {
			type : string
			text : { "and" }
		}
		
		2 :{
		
			type : action
			
			name : act_start_0
			
			tense : present_perfect
			
			verb_parameters : { }

			action_parameters : {
				subject : { "me" $coll.name$ }
				verb :{}
			}
			
			order : { subject verb }
			
		}
		
		3 :{
		
			type : action
			
			name : act_find_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }

			action_parameters : {
				subject : { "none" }
				verb :{}
				c_object : { "some interesting results" }
			}
			
			order : { verb c_object }
			
		}
		
		
		
	}

}




###################
comp_project_long_review_0 : {

	parameters : { proj coll }
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_think_0
			
			tense : present_simple
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_theme : { $proj.name$ }
			}
			
			order : { subject verb }
			
		}
		
		2 :{
		
			type : action
			
			name : act_be_0
			
			tense : infinite
			
			verb_parameters : { p0 : "would" }

			action_parameters : {
				subject : { "it" }
				verb :{}
				c_nominal : {"nice"}
			}
			
			order : { subject verb c_nominal}
			
		}
		
		3 :{
		
			type : action
			
			name : act_review_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }

			action_parameters : {
				subject : { "none" }
				verb :{}
				c_object : { "what" }
			}
			
			order : { verb c_object }
			
		}
		
		4 :{
		
			type : action
			
			name : act_do_0
			
			tense : present_perfect
			
			verb_parameters : { }
			
			person : 3

			action_parameters : {
				subject : { "we" }
				verb :{}
				c_object : { "so far" }
			}
			
			order : { subject verb c_object }
			
		}
		
		
		
	}

}

comp_project_long_review_1 : {

	parameters : { proj coll }
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_think_0
			
			tense : present_simple
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
			}
			
			order : { subject verb }
			
		}	
		
		1 : {
			type : string
			text : { "that" }
		}
		
		2 :{
		
			type : action
			
			name : act_be_0
			
			tense : infinite
			
			verb_parameters : { p0 : "should"}

			action_parameters : {
				subject : { "a meeting" }
				verb :{}
				c_nominal : {"arranged"}
			}
			
			order : { subject verb c_nominal }
			
		}
		
		3 :{
		
			type : action
			
			name : act_talk_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to"}

			action_parameters : {
				subject : { "none" }
				verb :{}
				c_theme : {"the project"}
			}
			
			order : { verb c_theme }
			
		}
		
	}

}

comp_project_long_review_2 : {

	parameters : { proj coll }
	
	structure : {
	
		0 :{
		
			type : action
			
			name : act_be_0
			
			tense : present_simple
			
			verb_parameters : { }
			
			person : 3

			action_parameters : {
				subject : { "we" }
				verb :{}
				c_place : {"the point"}
			}
			
			order : { subject verb c_place }
			
		}	
		
		1 : {
			type : string
			text : { "where" }
		}
		
		2 :{
		
			type : action
			
			name : act_plan_0
			
			tense : infinite
			
			verb_parameters : { p0 : "should" }
			
			person : 3

			action_parameters : {
				subject : { "we" }
				verb :{}
				c_object : {"how"}
			}
			
			order : { subject verb c_object }
			
		}
		
		3 :{
		
			type : action
			
			name : act_move_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }
			
			person : 3

			action_parameters : {
				subject : { "we" }
				verb :{}
				c_union : {"the work"}
				c_mode : {"forward"}
			}
			
			order : { verb c_mode c_union }
			
		}	
		
	}
}

comp_project_long_review_3 : {

	parameters : { proj coll }
	
	structure : {
		
		0 : {
			type : string
			text : { "in particular" }
		}
		
		1 : {
			type : string
			text : { \, }
		}
		
		2 :{
		
			type : action
			
			name : act_be_0
			
			tense : present_simple
			
			verb_parameters : {}
			
			person : 2

			action_parameters : {
				subject : { "some possible changes" }
				verb :{}
				c_nominal : {"there"}
			}
			
			order : { c_nominal verb subject }
			
		}
		
		3 :{
		
			type : action
			
			name : act_like_0
			
			tense : infinite
			
			verb_parameters : { p0 : "would" }
			

			action_parameters : {
				subject : { $sender$ }
				verb :{}
			}
			
			order : { subject verb }
			
		}
		
		4 :{
		
			type : action
			
			name : act_present_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }			

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object :{ $receiver$ }
			}
			
			order : { subject verb c_object }
			
		}
		
				
	}

}

comp_project_long_interact_0 : {

	parameters : { proj coll }
	
	structure : {
		
		2 :{
		
			type : action
			
			name : act_be_0
			
			tense : present_simple
			
			verb_parameters : {}			

			action_parameters : {
				subject : { $coll.name$ }
				verb :{}
				c_nominal :{ "of the same opinion" }
			}
			
			order : { subject verb c_nominal }
			
		}
		
		3 : {
			type : string
			text : { \, }
		}
		
		4 : {
			type : string
			text : { "and" }
		}
		
		5 :{
		
			type : action
			
			name : act_think_0
			
			tense : present_simple
			
			verb_parameters : { }			

			action_parameters : {
				subject : { $coll.name$ }
				verb :{}
			}
			
			order : {  verb }
			
		}
		
		6 : {
			type : string
			text : { "that" }
		}
		
		7 :{
		
			type : action
			
			name : act_like_0
			
			tense : infinite
			
			verb_parameters : { p0 : "would" }			

			action_parameters : {
				subject : { %pronouns.$coll.gender$.subjective% }
				verb :{}
				c_object : {"to" %coll_request.pos% }
			}
			
			order : { subject verb c_object }
			
		}
		
		8 : {
			type : string
			text : { "before" }
		}
		
		9 :{
		
			type : action
			
			name : act_add_0
			
			tense : continuous_form
			
			verb_parameters : {  }			

			action_parameters : {
				subject : { %pronouns.$coll.gender$.subjective% }
				verb :{}
				c_object : { "more features to the project" }
			}
			
			order : { verb c_object }
			
		}
		
		
				
	}

}

comp_project_long_interact_1 : {

	parameters : { proj coll }
	
	structure : {
		
		0 :{
		
			type : action
			
			name : act_think_0
			
			tense : present_simple
			
			verb_parameters : { p1 : "however" }			

			action_parameters : {
				subject : { $coll.name$ }
				verb :{}
			}
			
			order : { subject verb }
			
		}
		
		1 : {
			type : string
			text : { "that" }
		}
		
		3 : {
			type : string
			text : { \, }
		}
		
		4 : {
			type : string
			text : { "despite this" \, }
		}
		
		5 :{
		
			type : action
			
			name : act_be_0
			
			tense : present_simple
			
			verb_parameters : { p1 : "still" }

			person : 5

			action_parameters : {
				subject : { "some important issues" }
				verb :{}
				c_nominal : {"there"}
			}
			
			order : { c_nominal verb subject }
			
		}
		
		6 :{
		
			type : action
			
			name : act_solve_0
			
			tense : infinite
			
			verb_parameters : { p0 : "to" }

			action_parameters : {
				subject : { "none" }
				verb :{}
				c_time_limit : {"moving forward"}
			}
			
			order : { verb c_time_limit }
			
		}
		
		7 : {
			type : string
			text : { \, "and" }
		}
		
		8 :{
		
			type : action
			
			name : act_want_0
			
			tense : present_simple
			
			verb_parameters : { p1 : "to" }

			action_parameters : {
				subject : { $coll$ }
				verb :{}
				c_object : {%coll_request% " first"}
			}
			
			order : { verb c_object }
			
		}
		
				
	}

}

comp_project_long_interact_2 : {

	parameters : { proj coll }
	
	structure : {
		
		0 :{
		
			type : action
			
			name : act_think_0
			
			tense : present_simple
			
			verb_parameters : {  }			

			action_parameters : {
				subject : { $coll.name$ }
				verb :{}
			}
			
			order : { subject verb }
			
		}
		
		1 : {
			type : string
			text : { "that" }
		}


		2 :{
		
			type : action
			
			name : act_be_0
			
			tense : present_simple
			
			verb_parameters : { }			

			action_parameters : {
				subject : { "it" }
				verb :{}
				c_nominal : {"important"}
			}
			
			order : { subject verb c_nominal }
			
		}

		3 : {
			type : string
			text : { "to" %coll_request% \, }
		}
		
		4 : {
			type : string
			text : { "before" }
		}
		
		5 :{
		
			type : action
			
			name : act_make_0
			
			tense : present_simple
			
			person : 3
			
			verb_parameters : { }			

			action_parameters : {
				subject : { "we" }
				verb :{}
				c_object : {"any more changes"}
			}
			
			order : { subject verb c_object }
			
		}
				
	}
}