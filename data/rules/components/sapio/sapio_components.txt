# SAPIO COMPONENTS

comp_sapio_request_time_0 : {

	parameters : {}
	
	structure : {
	
		0 : {
			type : string
			text : { "could" }
		}
	
		1 :{
		
			type : action
			
			name : act_move_0
			
			tense : infinite
			
			verb_parameters : { }

			action_parameters : {
				subject : { "we" }
				verb :{}
				c_object : { "the next meeting" }
				c_origin : { "tomorrow" }
				c_destination : { "the day after" }
			}
			
			order : { subject verb c_object c_destination }
			
		}
		
	}

}

comp_sapio_reason_0 : {

	parameters : {}
	
	structure : {
	
		0 : {
			type : string
			text : { "since" }
		}
	
		1 :{
		
			type : action
			
			name : act_be_0
			
			tense : infinite
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_nominal : { "a bit busy" }
				c_time_state : { "tomorrow" }
			}
			
			order : { subject verb c_nominal c_time_state }
			
		}
		
		2 : {
			type : string
			text : { "?" }
		}
		
	}

}

comp_sapio_appease_0 : {

	parameters : {}
	
	structure : {
	
		0:{
		
			type : action
			
			name : act_hope_0
			
			tense : present_simple
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_object : { "this is not a problem" }
			}
			
			order : { subject verb c_object }
			
		}
		
		1 : {
			type : string
			text : { ", otherwise" }
		}
	
		2 :{
		
			type : action
			
			name : act_be_0
			
			tense : present_simple
			
			verb_parameters : { }

			action_parameters : {
				subject : { $sender$ }
				verb :{}
				c_nominal : { "OK" }
				c_relation : { "leaving it on the same day" }
			}
			
			order : { subject verb c_nominal c_relation }
			
		}
		
		3 : {
			type : string
			text : { "." }
		}
		
	}

}

