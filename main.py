# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 09:19:44 2020

@author: Ale
"""

import sys
import random
from copy import deepcopy

from utils import collection_copy

from parser_utils import generate_verbs_structure, generate_tenses_structure
from parser_utils import generate_components_structure, generate_segments_structure
from parser_utils import generate_actions_structure, generate_categories_structure
from parser_utils import generate_special_symbols_structure

from new_parser import mail_tree, mail_tree_recurse, process_subcomponent
from new_parser import process_mail_subject, process_punctuation

# Maximum recursive steps for a mail segment (to be moved to INI file)
MAX_RECURSION_DEPTH = 50

# VERBPATH: The path to the file with the verb rules
# TENSEPATH: The path to the file with tenses rules
# ACTIONPATH: The path to the file with action rules
# COMPDIR: The path to the file with the list of components
# MAILPATH: The path to the file with the list of mail sections
# SAVEPATH: The path to the file where to save the generated mails
VERBPATH = r".\data\rules\verbs"
TENSEPATH = r".\data\rules\tenses"
ACTIONPATH = r".\data\rules\actions"
COMPDIR = r".\data\rules\components"
MAILPATH = r".\data\rules\segments"
CATSDIR = r".\data\categories"
SYMDIR = r".\data\symbols"
SAVEPATH = r"output4.txt"

# Base tenses required to be present in each verb definition in the verb file,
# otherwise it will be impossible to form compound tenses
REQUIREMENTS = ["present", "past_simple", "perfect", "continuous"]        

# TESTING PORTION
# NOTE: All of the testing (and result giving) portion is kept outside of a
# function, even the main, for personal preference: my development environment
# allows me to explore every variable declared outside of a function, which is
# instrumental for quick testing and bug-fixing. This should not impact any
# functionality in other environments

# Pregenerating people to use
people = [
      {"id":"0",
      "name":"John",
      "surname":"Smith",
      "gender":"male",
      "company":"Fictitious Inc.",
      "position":"PR"},    
      {"id":"1",
      "name":"Bob",
      "surname":"Ross",
      "gender":"male",
      "company":"AWATO Corp.",
      "position":"Accountant"},
       {"id":"2",
      "name":"Alessandro",
      "surname":"Maccagno",
      "gender":"male",
      "company":"SW Inc.",
      "position":"CEO"},
      {"id":"3",
      "name":"Francesco",
      "surname":"Sapio",
      "gender":"male",
      "company":"Fictitious Inc.",
      "position":"PR"},    
      {"id":"4",
      "name":"Massimo",
      "surname":"Mecella",
      "gender":"male",
      "company":"Sapienza",
      "position":"Professor"}
]

colls = [{"id":"5",
      "name":"Eleonora",
      "surname":"NOLASTNAME",
      "gender":"female",
      "company":"",
      "position":""},    
      {"id":"6",
      "name":"Lauren",
      "surname":"Ferro",
      "gender":"female",
      "company":"",
      "position":""},    
      {"id":"7",
      "name":"Silvestro",
      "surname":"NOLASTNAME",
      "gender":"male",
      "company":"",
      "position":""},    
      {"id":"8",
      "name":"Barbara",
      "surname":"Weber",
      "gender":"female",
      "company":"",
      "position":""}]

# Name of the segment to start the mail generation with
MAILSTART = "seg_mecella_long_proj_start" # "seg_mecella_start"

projs = ["FIRST", "ARCA", "NOTAE", "GEA2", "STORYBOOK", "AWATO"]
proj_sample = {"name":projs[random.randint(0, len(projs)-1)], "type":"PROJTYPE"}


if False:
    sys.exit()

# Structure creation section
verb_structure = generate_verbs_structure(VERBPATH, REQUIREMENTS)
tense_structure = generate_tenses_structure(TENSEPATH)
action_structure = generate_actions_structure(ACTIONPATH)
components_structure = generate_components_structure(COMPDIR)
mail_structure = generate_segments_structure(MAILPATH)
category_structure = generate_categories_structure(CATSDIR)
symbols_structure = generate_special_symbols_structure(SYMDIR)

# Mail tree section
mail_dictionary = mail_tree(mail_structure)

all_mails = []
all_subjects = []

threshold = 1

for i in range(10000):
        
    data = {"sender":people[3], "receiver":people[4], "p1": colls[random.randint(0,len(colls)-1)], "p2":proj_sample}
    
    # Develop the mail tree to its base components
    full_mail_raw, mail_subject_raw = mail_tree_recurse(MAILSTART, mail_structure, mail_dictionary, MAX_RECURSION_DEPTH)
    full_mail = collection_copy(full_mail_raw)
    complete_mail_string = ""
    
    if mail_subject_raw != tuple():
        mail_subject = process_mail_subject(mail_subject_raw[0], mail_subject_raw[1], data, category_structure)
        mail_subject_punctuated = process_punctuation(mail_subject, symbols_structure)
        all_subjects.append(mail_subject_punctuated)
    else:
        all_subjects.append("")
    
    for mail_part in full_mail:
                
        # Check that every element in the final mail parts list is a component,
        # because component parts are the only terminal elements
        assert mail_part["type"][0] == "component"    
        
        component_name = mail_part["structure"]['0']['component'][0]
        component_pars = mail_part["structure"]['0']['parameters']
        
        action_subelements = components_structure[component_name]["structure"]
            
        # List of final strings associated to subcomponents and sentence associated
        # to single component
        processed_subcomponents_list = []
        sentence = ""
        
        sorted_subcomponents = [action_subelements[x] for x in sorted(list(action_subelements.keys()))]   
        
        # Obtain the final string associated with every component
        for subcomponent in sorted_subcomponents:
            processed_subcomponent = process_subcomponent(subcomponent, action_structure, verb_structure, tense_structure, data, category_structure, component_pars)
            processed_subcomponents_list.append(processed_subcomponent)
                    
        # All final strings associated to subcomponents are joined together
        for subcomponent_string in processed_subcomponents_list:
            sentence += subcomponent_string + " "
        
        # Cobbled together method to remove unaesthetic results. Will require a
        # more precise solution for real and optimal results

        # Spaces after newlines should be solved with punctuation elements
        sentence = sentence.replace("\n ", "\n")
        complete_mail_string += sentence
        
    complete_mail_string = process_punctuation(complete_mail_string, symbols_structure)
                
    # print(complete_mail_string)
    if complete_mail_string not in all_mails:
        all_mails.append(complete_mail_string)
    if len(all_mails) >= threshold:
        break
   
if False:
    f = open(SAVEPATH, "w")
    for m in all_mails:
        sep = "0========================\n"
        f.write(str(m))
        f.write('\n')
        f.write(sep)
    f.close()
    
print(all_subjects[0] + '\n\n' + all_mails[0])
