# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 19:20:09 2020

@author: Ale
"""

import random
from utils import randkey, collection_copy
from category_reader import process_category # get_category_element

# A function to process elements of a component action (subject or complement).
# @param header - string, the label of the component
# @param element - list, the body of the element
# @param data - dictionary, the parameters of the action (sender, receiver, ...)
# @param segment_mapping - dictionary, records the mappings between current
#   parameters and the ones used by the segment which started the mail tree
# @return - list, the list of all subelements of the element, mapped to the
#     value they are supposed to have in the mail itself # TODO finish comments
def process_component_element(header, element, data, categories, segment_mappings={}):    
    
    assert type(element) == list
    
    subelement_list = []
    
    for subelement in element:
                
        # Parameter case
        if subelement[0] == subelement[-1] == "$":
            
            # Use the strip after removing the dollar signs, as spaces are not
            # counted as part of a non-quote element
            subelement = subelement[1:-1].strip()
            
            # Entity property case
            if '.' in subelement:
                [s0, s1] = subelement.split('.')
                
                # In case the original segment had parameters, we must check
                # what original name the parameter "inherited" from the section
                # which started the mail (except for 'sender'/'receiver', which
                # are universal)
                if s0 not in ["receiver", "sender"]:
                    parameter_match = segment_mappings[s0][0].strip()
                    parameter_match = parameter_match[1:-1]
                    subelement_list.append(data[parameter_match][s1])
                else:
                    subelement_list.append(data[s0][s1])
            
            else:
                
                # If the subelement is not in data, it means that it must be
                # remapped
                if subelement not in data:
                    subelement = segment_mappings[subelement][0].strip()[1:-1]
                
                # Use the correct pronoun, depending on the role and person
                # TODO: plural cases
                if header == "subject":
                    if subelement == "sender":
                        subelement_list.append("I")
                    elif subelement == "receiver":
                        subelement_list.append("you")
                    elif data[subelement]["gender"] == "male":
                        subelement_list.append("he")
                    elif data[subelement]["gender"] == "female":
                        subelement_list.append("she")
                    else:
                        subelement_list.append("them")
                else:
                    if subelement == "sender":
                        subelement_list.append("me")
                    elif subelement == "receiver":
                        subelement_list.append("you")
                    elif data[subelement]["gender"] == "male":
                        subelement_list.append("him")
                    elif data[subelement]["gender"] == "female":
                        subelement_list.append("her")
                    else:
                        subelement_list.append("them")
        
        # Category case
        elif subelement[0] == subelement[-1] == '%':
            
            subelement = subelement[1:-1].strip()            
            subelement_list.append(process_category(subelement, data, categories, segment_mappings))
        
        # String case
        elif subelement[0] == subelement[-1] == '"':
            subelement_content = subelement[1:-1]
            if len(subelement_content) > 0:
                subelement_list.append(subelement_content)
        
        else:
            subelement_list.append(subelement)
    
    # Format the subject to look better
    if header == "subject" and len(element) > 1:
        flag_sum = False    # Check if a '+' was just encountered
        single_subject = ""
        for i in range(len(subelement_list)):
            
            # '+' signs are used to join words which shouldn't be considered as
            # different entities
            if subelement_list[i] == '+':
                flag_sum = True
                continue
            
            # Different entities case
            else:
                if not flag_sum:
                    if i == 0:
                        single_subject += subelement_list[i]                    
                    else: 
                        # Put a comma if there's more than one to be listed 
                        if i < len(subelement_list)-2:
                            single_subject += ", " + str(subelement_list[i])
                        # Use 'and' otherwise
                        else:
                            single_subject += " and " + str(subelement_list[i])
                else:
                    single_subject += subelement_list[i]
                    flag_sum = False
# =============================================================================
#                 single_subject += str(subelement_list[i])
#                 if not flag_sum:
#                     if i < len(subelement_list)-2:
#                         single_subject += ", "
#                     else:
#                         single_subject += " and "
# =============================================================================
    
        subelement_list = [single_subject]
    
    return subelement_list



# The function returns which one of the 6 verbal persons the verb must use,
# depending on the subject of the sentence
# TODO: Plural forms (not necessary I think)
# @param subject - list, list of all entities being subjects of the sentence
# @return - integer, the integer 0-5 representing the verbal person
def determine_verb_person(subject):    
    
    if len(subject) == 1:
        
        subject = subject[0]
                
        if subject[0] == subject[-1] == "$":
            
            subject = subject[1:-1].strip()
            
            if subject == "sender":
                return 0
            elif subject == "receiver":
                return 1
            else:
                return 2
        elif subject[0] == subject[-1] == '"' or subject[0] == subject[-1] == '%':
            return 2
    else:
                
        has_sender = False
        has_receiver = False
        
        for subject_element in subject:
            if subject_element[0] == subject_element[-1] == '$':
                subject_element_inside = subject_element[1:-1]
                if subject_element_inside == "sender":
                    has_sender = True
                elif subject_element_inside == "receiver":
                    has_receiver = True
        
        if has_sender:
            return 3
        elif not has_sender and has_receiver:
            return 4
        else:
            return 5
    


# This function is used to to form the correct verb tense which will be used by
# the action
# @param verb - string, the name of the verb of the action
# @param  tense - list, the list of words and parameters which form the verb
# @param  person - integer, the verbal person (0-5) of the verb
# @param  params - dictionary: the parameters that the verb uses
# @param  verbs_struct - dictionary: the structure containing all of the verbs
# @param is_infinite - boolean, whether the complete tense is infinite or not
# @return - list, the list containing all of the consecutive words that make up
#     the verb with the given tense, person and parameters
def process_tense(verb, tense, person, params, verbs_struct, is_infinite):
    
    tense_list = []
            
    # Loop over all the components (verbal forms and parameters) of a tense    
    for tense_element in tense:
                
        # The angular brackets identify verbal forms/tenses
        if tense_element[0] == '<' and tense_element[-1] == '>':
            
            # Base tenses follow the format 'verb.tense'
            tense_element = tense_element[1:-1].strip()
            [e0, e1] = tense_element.split('.')
            
            # The infinite form is obtained from the verb ('infinite_id'), so
            # it cannot be obtained by simply splitting the tense element via
            # the dot in case the verb in question is the main verb of the action
            # (which is represented as '<verb.base_tense>', without specifying
            # the verb in question)
            if e1 == "infinite":
                if is_infinite and 'p0' in params and params['p0'][0] != "":
                    tense_list.append(params['p0'][0][1:-1])
                                        
                tense_list.append(verb.split('_')[0])
                
                if is_infinite and 'p1' in params and params['p1'][0] != "":
                    tense_list.append(params['p1'][0][1:-1])
            else:
                
                # As explained before, the main verb of an action is expressed
                # as 'verb', unlike the auxiliary forms accompanying it in
                # certain tenses
                if e0 == "verb":
                    e0 = verb                
                verb_base_form = verbs_struct[e0][e1]
                                
                # The base form of a verb could either have a single element
                # (like the participle/continuous form) or a potentially different
                # one for each verbal person (like the present)
                if len(verb_base_form) == 1:
                    tense_list.append(verb_base_form[0])
                else:
                    tense_list.append(verb_base_form[person])
                    
        # Parameters are delimited by dollar signs
        elif tense_element[0] == tense_element[-1] == '$':
            
            # Spaces preceding or trailing the name of a parameter are not
            # counted as part of the name itself, so they are removed after
            # exciding the dollar sign delimiters
            tense_element = tense_element[1:-1].strip()
            if tense_element in params:
                for tense_params_element in params[tense_element]:
                    tense_list.append(tense_params_element[1:-1])
        
        # String case (text in quote)
        else:
            tense_list.append(tense_element[1:-1])
                    
        
    return tense_list


def process_word(word, data, categories, segment_mappings={}):
    
    # Parameter case
    if word[0] == word[-1] == "$":
        word_inside = word[1:-1]
        
        # Check that the parameter is not empty
        if len(word_inside) == 0:
            return ""   
        
        
        if '.' in word_inside:
            [e0, e1] = word_inside.split('.')
            if e0 in data:
                word_out = data[e0][e1]
                return word_out
            elif e0 in segment_mappings:
                word_out = data[segment_mappings[e0]][e1]
                return word_out
            else:
                return ""
    
    # Category case
    elif word[0] == word[-1] == "%":
        word_out = process_category(word_inside, data, categories, segment_mappings)
        return word_out
    
    # Strings and pure words
    else:
        return word    
    
    return

# This function returns the string relative to a mail subject element
# @param mail_subject - list, list of all the elements making up the subject
# @param map_snapshot - structure, map of the parameters to the original
#   parameters, at the step in which the mail subject was acquired
# @param data - dictionary, contains all of the data (sender, receiver, parameters)
#     necessary obtain text from the subcomponent structure
# @param categories - dictionary, contains all of the categories with labels
# @return - string, the string corresponding to the mail subject
def process_mail_subject(mail_subject, map_snapshot, data, categories):
    
    subject_string = ""
    
    for element in mail_subject:
        
        # String case
        if element[0] == element[-1] == '"':
            element_inside = element[1:-1]
            subject_string += element_inside
        elif element[0] == element[-1] == '$':  # Parameter case
            element_parameter = element[1:-1]
            
            if '.' in element_parameter: # Field of a parameter
                e0, e1 = element_parameter.split('.')
                
                if e0 in ["sender", "receiver"]:
                    subject_string += data[e0][e1]
                else:
                    subject_string += data[map_snapshot[e0]][e1]
            
            else:
                if element_parameter in ["sender", "receiver"]:
                    print("ERROR - Trying to use pure sender/receiver")
                    subject_string += data[element_parameter]["name"]
                else:
                    subject_string += data[map_snapshot[element_parameter]]
        elif element[0] == element[-1] == '%':
            category_string = process_category(element[1:-1], data, categories, map_snapshot)
            subject_string += category_string
        else:
            subject_string += element
            
            
    
    return subject_string

# This function takes a subcomponent (action/string elements part of a component)
# and obtains the string which will represent it in the mail.
# @param subcomponent - dictionary, subcomponent structure
# @param actions_struct - dictionary, actions structure
# @param verbs_struct - dictionary, verbs structures
# @param tenses_struct - dictionary, tenses structure
# @param data - dictionary, contains all of the data (sender, receiver, parameters)
#     necessary obtain text from the subcomponent structure
# @return - string, the text which will represent this subcomponent and that
#     will be inserted into the mail
def process_subcomponent(subcomponent, actions_struct, verbs_struct, tenses_struct, data, categories, segment_mappings={}):
        
    subcomponent_type = subcomponent["type"][0]
    subcomponent_list = []
    subcomponent_string = ""
        
    #Action case
    if subcomponent_type == "action":
        
        # First extract all fields of the action subcomponent
        action_name = subcomponent["name"][0]
        action_parameters = subcomponent["action_parameters"]
        action_order = subcomponent["order"]
        action_tense = subcomponent["tense"][0]
        verb_parameters = subcomponent["verb_parameters"]  
        
        
        # All verbs which can be used by an action are equally possible and
        # acceptable, so just select one randomly to add variation in the content
        action_verb_head = randkey(actions_struct[action_name]) 
        action_verb_body = actions_struct[action_name][action_verb_head]
        action_verb_logics = action_verb_body["logics"]
        
        action_verb_person = -1
        
        # Check if the person was not specified
        if "person" in subcomponent:
            action_verb_person = int(subcomponent["person"][0])
        else:
            for element in action_parameters:
                if element == "subject":                         
                    action_verb_person = determine_verb_person(action_parameters[element])       
                
        # The only elements bound to appear are the ones in the aciton order;
        # other elements can still be stated in the action_parameters
        for element in action_order:
            if element != "verb":
                
                subcomponent_element_string = ""             
                
                # Check if the element requires a preposition
                if element in action_verb_logics:
                    subcomponent_element_string += str(action_verb_logics[element][0][1:-1]) + " "
                    
                # The element is processed to obtain its final counterpart to
                # insert in the mail
                if element != "":
                    element_action_parameters = action_parameters[element]
                processed_component_element = process_component_element(element, element_action_parameters, data, categories, segment_mappings)                
                
                # Since it is impossible to intervene and selectively add string
                # content at this step, elements which are represented with
                # multiple words, and multiple elements, have their associated
                # strings automatically separated by spaces
                for component_element in processed_component_element:
                    subcomponent_element_string += str(component_element) + " "
                    
                # Remove the inevitable trailing space, as its at the end of
                # the string connected to the action, where it is possible to
                # format it and add any necessary space
                subcomponent_element_string = subcomponent_element_string.strip()
                
                subcomponent_list.append(subcomponent_element_string)
            else:
                
                is_infinite = False
                
                # Since verbs' infinite forms are obtained from the name they
                # are saved as on file, they don't appear in the verbs/tenses
                # structures and must be signalled in their own way
                if action_tense != "infinite" and action_verb_person != -1:
                    action_tense_struct = tenses_struct[action_tense]["structure"]
                else:
                    action_tense_struct = ["<verb.infinite>"]
                    is_infinite = True
                                    
                # Obtain the string associated to the verb
                processed_tense = process_tense(action_verb_head, action_tense_struct, action_verb_person, verb_parameters, verbs_struct, is_infinite)
                for processed_tense_element in processed_tense:
                    subcomponent_list.append(processed_tense_element)

        
    elif subcomponent_type == "string":       
        
        for word in subcomponent["text"]:
                    
            if word[0] == word[-1] == "$":
                word = word[1:-1].strip()
                
                # String MUST refer a field of an entity, not an entity by
                # itself, as it is a virtual structure with no "word" equivalent
                # by itself
                assert '.' in word
                
                [s0, s1] = word.split('.')
                subcomponent_list.append(data[s0][s1])
            elif word[0] == word[-1] == '%':
                word = word[1:-1].strip()            
                subcomponent_list.append(process_category(word, data, categories, segment_mappings))
            elif word[0] == word[-1] == '"':
                subcomponent_list.append(word[1:-1].strip())
            elif word[0] == "\\":
                if word == "\\n":
                    subcomponent_list.append('\n')
                elif word == "\\.":
                    subcomponent_list.append(word)
                else:   # ??
                    subcomponent_list.append(word)
                    
    else:
        print("UNRECOGNIZED")
        return [None]
    
    # Concatenate all words obtained in this function
    for word in subcomponent_list:
        subcomponent_string += word + " "
    
    # Remove the inevitable trailing space selectively (the 'strip' function
    # would also remove eventual desired spaces or newlines)
    subcomponent_string = subcomponent_string[:-1]
    
    return subcomponent_string




# This function generates a "mail tree", where each mail section lists the
# sections which in turn form it. These sections can be either classified as
# "sections" proper, which represent intermidiate, high-level parts of a mail,
# "components", which contain references and parameters for mail components,
# and finally classes, which group sets of similar and interchangeable components
# @param mail_struct - dictionary, the structure containing all mail sections
# @return - dictionary, contains all of the "sections"/"components" elements
#     plus what elements (including components) they are formed by.
def mail_tree(mail_struct): #TODO: MODIFICATIONS APPLIED, FIX COMMENTS
    
    mail_dict = {}
    
    for mail_part in mail_struct:
        
        # Every mail part must be uniquely named
        assert mail_part not in mail_dict
        
        mail_dict[mail_part] = []
        mail_part_body = mail_struct[mail_part]
        
        # Every mail part must have a type
        assert "type" in mail_part_body
        
        mail_part_type = mail_part_body["type"][0]
        
        if mail_part_type == "section":
            
            # The keys of the mail_part_body["structure"] structure are the
            # order in which the children of the section are bound to appear.
            # They are converted to integer to sort them correctly
            list_of_parts = list(mail_part_body["structure"].keys())
            list_of_parts_int = [int(x) for x in list_of_parts]
            list_of_parts_int = sorted(list_of_parts_int)
            
            # Section have multiple "children", all guaranteed to appead
            for part_component in list_of_parts_int:
                
                # The header for the section children must be converted back
                # to string, since that's the type any character read from
                # file is assigned (even numbers)
                part_component_body = mail_part_body["structure"][str(part_component)]
                
                # Every child is saved along with its type; the type is
                # currently not used, so it will probably be removed in a
                # future revision
                if "section" in part_component_body:
                    mail_dict[mail_part].append(part_component_body)
                elif "component" in part_component_body:
                    mail_dict[mail_part].append(part_component_body)
                else:
                    mail_dict[mail_part].append(part_component_body)
         
        # Component-type parts may not have children, so they don't get included
        elif mail_part_type == "class":   
            
            for class_child in mail_part_body["structure"]:
                
                
                class_child_element = mail_part_body["structure"][class_child]
                # class_child_type = class_child_element["type"][0]
                class_child_name = class_child_element["name"][0]
                                
                mail_dict[mail_part].append(mail_struct[class_child_name])
            
        
    return mail_dict

# =============================================================================
# Recursively explore the mail tree to form a complete mail. This is started by
# feeding an initial mail part (which will work as a sort of identifier to
# decide which class of mail will be generated), and it will be gradually
# expanded and made more thorough via the mail tree evolution rules, until
# it is completely made up of components part (which are terminal elements for
# the evolution rules)
# @param part - string, the mail part's name (the same used in the rules file)
# @param mail_struct - dictionary, structure containing all of the mail sections,
#     with their respective parameters and components
# @param mail_dict - dictionary, the mail tree
# @param max_depth - integer, the maximum depth reachable before cutoff (to
#   avoid infinite recursion)
# @return - the list of mail components, which will be then taken by the
#     component processing functions to obtain the strings which will form
#     the mail    
# =============================================================================
def mail_tree_recurse(part, mail_struct, mail_dict, max_depth=999):  
    
    # TODO add comments
    
    def mail_tree_recurse_inside(part, parameter_map, mail_struct, mail_dict, max_depth=999, current_depth=0):
        
        # Depth check to prevent infinite recursion
        if current_depth == max_depth:
            return "", ""
                
        part_type = mail_struct[part]["type"][0]
                
        # Component parts are terminal, and are therefore the base step
        if part_type == "component":
            
            component_parameters = mail_struct[part]["parameters"]            
            new_component_parameters_list = []
            new_component_parameters = {} 
            
            new_segment = collection_copy(mail_struct[part])
            
            # Remap parameters # TODO (should work now)
            if len(component_parameters) > 0:
                
                for component_parameter in component_parameters:
                    new_component_parameters_list.append(parameter_map[component_parameter][0][1:-1])
                    
                actual_component = mail_struct[part]["structure"]['0']
                actual_component_parameters = actual_component["parameters"]
                
                for actual_component_parameter in actual_component_parameters:  
                    
                    component_parameter = actual_component_parameters[actual_component_parameter]
                    
                    assert len(component_parameter) == 1
                    component_parameter = component_parameter[0]                
                    
                    assert component_parameter[0] == component_parameter[-1] == '$'
                    component_parameter = component_parameter[1:-1]
                    
                    new_component_parameter = parameter_map[component_parameter]
                    new_component_parameters[actual_component_parameter] = new_component_parameter 
                                
                new_segment["parameters"] = new_component_parameters_list
                new_segment["structure"]['0']["parameters"] = new_component_parameters
             
            return [new_segment], tuple()
        
        elif part_type == "class":      
            
            # Select a random component part of all those under the same class
            n_element_in_class = len(mail_struct[part]["structure"])
            random_idx = random.randint(0, n_element_in_class-1)
            random_component = list(mail_struct[part]["structure"].keys())[random_idx]
            random_component_name = mail_struct[part]["structure"][random_component]["name"][0]
            
            # Extract the parameters of the selected child (need only those
            # since only one child is going to be developed)
            child_structure = mail_struct[part]["structure"][random_component]
            child_structure_parameters = child_structure["parameters"]
                        
            new_child_parameters = {}
            
            # Don't perform mapping operations if there's no parameters to
            # map
            if len(child_structure_parameters) > 0:
                
                for child_parameter in child_structure_parameters:
                                        
                    child_parameter_match = child_structure_parameters[child_parameter]
                    
                    assert len(child_parameter_match) == 1
                    child_parameter_match = child_parameter_match[0].strip()
                    
                    assert child_parameter_match[0] == child_parameter_match[-1] == '$'
                    child_parameter_match = child_parameter_match[1:-1]                    
                                            
                    #new_child_parameter = str(parameter_map[child_parameter_match][0][1:-1])
                    #new_child_parameters[child_parameter] = [new_child_parameter]
                
                # Prevent side effects
                new_child_parameters = collection_copy(child_structure_parameters)       
                
                # Remap the parameters of the children if the class already had
                # to be remapped
                if len(parameter_map) > 0:
                                        
                    for new_parameter in new_child_parameters:
                                                    
                        new_parameter_match = new_child_parameters[new_parameter][0][1:-1]
                                                         
                        assert new_parameter_match in parameter_map                                                        
                        new_child_parameters[new_parameter] = parameter_map[new_parameter_match]
                        
            mail_subject_tuple = tuple()
            
            # Mail subject, after the parameter map operation since the
            # potentially updated map is required
            # TRY THE SNAPSHOT APPROACH
            if "mail_subject" in mail_struct[part]:
                mail_subject_copy = collection_copy(mail_struct[part]["mail_subject"])         
                parameter_map_copy = collection_copy(parameter_map)
                mail_subject_tuple = (mail_subject_copy, parameter_map_copy)               
             
            # Subparts follow the format (part_type, part_name), hence the [1] # ???
            recursion_ret, recursion_subject = mail_tree_recurse_inside(random_component_name, new_child_parameters, mail_struct, mail_dict, max_depth, current_depth+1)                
            
            if recursion_subject != tuple():
                mail_subject_tuple = recursion_subject  
            
            return recursion_ret, mail_subject_tuple
        
        else:   # Section case     
            
            mail_subject_tuple = tuple()
                        
            mail_list = []            
            for subpart in mail_dict[part]:
                
                new_parameter_map = {}
                                
                subpart_name = subpart["segment"][0]
                subpart_parameters = subpart["parameters"]
                                
                # Don't perform mapping operations if there's no parameters to
                # map
                if len(subpart_parameters) > 0:
                                        
                    for subpart_parameter in subpart_parameters:
                        
                        subpart_parameter_body = subpart_parameters[subpart_parameter]
                        
                        # Each parameter can be mapped to one and only one
                        # parameter
                        assert len(subpart_parameter_body) == 1
                        subpart_parameter_body = subpart_parameter_body[0].strip()
                        
                        # Parameters are delimited by the '$', as usual
                        assert subpart_parameter_body[0] == subpart_parameter_body[-1] == '$'
                        subpart_parameter_body = subpart_parameter_body[1:-1]
                                                
                        subpart_parameter_body = '$' + str(subpart_parameter_body) + '$'
                        new_parameter_map[str(subpart_parameter)] = [str(subpart_parameter_body)]
                        
                    # If there's already a mapping being passed down, use it to
                    # map the current "new" parameters to the one being passed
                    if len(parameter_map) > 0:
                        
                        for new_parameter in new_parameter_map:
                                                        
                            new_parameter_match = new_parameter_map[new_parameter][0][1:-1]                            
                            assert new_parameter_match in parameter_map                                                        
                            new_parameter_map[new_parameter] = parameter_map[new_parameter_match] 
                
                                
                # Mail subject, after the parameter map operation since the
                # potentially updated map is required
                # TRY THE SNAPSHOT APPROACH
                if "mail_subject" in mail_struct[part]:
                    mail_subject_copy = collection_copy(mail_struct[part]["mail_subject"])         
                    parameter_map_copy = collection_copy(parameter_map)
                    mail_subject_tuple = (mail_subject_copy, parameter_map_copy)
                               
                                                         
                # Subparts follow the format (part_type, part_name), hence the [1] # ???
                recursion_ret, recursion_subject = mail_tree_recurse_inside(subpart_name, new_parameter_map, mail_struct, mail_dict, max_depth, current_depth+1)                
                
                if recursion_subject != tuple():
                    mail_subject_tuple = recursion_subject
                    
                 # Every return of a recursive step is a list
                if type(recursion_ret) == list:
                    for x in recursion_ret:
                        if x != "":
                            mail_list.append(x)
            
            return mail_list, mail_subject_tuple
    
    # END OF THE INTERNAL AUXILIARY FUNCTION
    
    mail_list, mail_subject = mail_tree_recurse_inside(part, {}, mail_struct, mail_dict, max_depth, 0)
        
    return mail_list, mail_subject

# Dynamically process the punctuation in a mail string
# @param mail_string - string, the string representation of the mail
# @param symbols_structure - dictionary, the sctructure with all punctuation
#   symbols
# @return - string, the processed text
def process_punctuation(mail_string, symbols_structure):
    
    complete_mail_string = mail_string
    
    for symbol in symbols_structure:
        
        original_symbol = symbols_structure[symbol]['original_character'][0].strip()
        if original_symbol[0] == original_symbol[-1] == '"':
            original_symbol = original_symbol[1:-1]
        
        final_symbol = symbols_structure[symbol]['actual_character'][0].strip()
        if final_symbol[0] == final_symbol[-1] == '"':
            final_symbol = final_symbol[1:-1].strip()
        
        symbol_tags = symbols_structure[symbol]['tags']
        original_symbol_length = len(original_symbol)
           
        while True:
            punct_position = complete_mail_string.find(original_symbol)
            if punct_position == -1:
                break
            pre_string = complete_mail_string[:punct_position]
            post_string = complete_mail_string[punct_position+original_symbol_length:]
            
            if "remove_pre_spaces" in symbol_tags:
                while len(pre_string) > 0 and pre_string[-1] == " ":
                    pre_string = pre_string[:-1]
            
            if "causes_capitalization" in symbol_tags:
                for cid, char in enumerate(post_string):
                    if char in [' ', '\n']:
                        continue
                    if char.islower():
                        post_string = post_string[:cid] + str(char).upper() + post_string[cid+1:]
                        break
                    else:
                        break
                    
            if "requires_post_space" in symbol_tags:
                if len(post_string) == 0 or post_string[0] != " ":
                    post_string = ' ' + post_string
                    
            complete_mail_string = pre_string + str(final_symbol) + post_string
    
    return complete_mail_string
    