# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 19:46:05 2020

@author: Ale
"""

import os

# A function to linearize a file, in order to deliver its content on a string
# without newline characters. The file still has to obey the rules set for the
# LANGUAGE, meaning that an hash symbol will be regarded as the beginning of a
# comment, and will cause this function to ignore aything between it and the
# next newline.
# @param filepath - string, the path to the file to linearize
# @return - string, the linearized file, with the comments removed
def linearize_file(filepath):
    
    # Open the file, save its line and then close it immediately, in order not
    # to have to access it again; done to improve efficiency
    f = open(filepath, 'r')
    lines = f.readlines()
    f.close()
    
    # Only save lines with non-zero length and which don't start with an hash
    # to remove edge cases from the get-go.
    # Both len() and line[0] checks are O(1), so this operation is relatively
    # inexpensive.
    usable_lines = []
    for line in lines:
        
        # Done to prevent leading/trailing spaces from preventing the check
        # on line length and first character from properly working; this
        # also immediately removes unnecessary spaces, which have no value in
        # LANGUAGE when leading/trailing strings
        clean_line = line.strip()
        
        if len(clean_line) > 0 and clean_line[0] != "#":
            usable_lines.append(clean_line)
    
    # Removal of the commented part of a line (if present)
    actual_length_lines = []
    for line in usable_lines:
        partial_line = ""
        for char in line:
            if char == '#':
                break
            else:
                partial_line += char
        actual_length_lines.append(partial_line)
            
    # Concatenate all of the lines and remove surviving leading/trailing spaces
    single_line_file = ""
    for line in actual_length_lines:
        single_line_file += str(line) + " "
        
    single_line_file = single_line_file.strip()
    
    return single_line_file



# The main function to universally parse any file written in LANGUAGE.
# It can return two types of structures: a list of values or a structure with
# its own (potential) substructures; they are mutually exclusive.
# @param single_line_file - string, the file to parse contained in a string with
#     no newline characters
# @return - dictionary, the contents of the file organized in a structure, as per
#     LANGUAGE conventions
def parse_verb_language(single_line_file):
     
    verb_struct = {}
    words_list = []
       
    
    # The space character is used to separate different elements of the file, so
#     an extra one is added to prompt a closure of any structure being analyzed
#     without having to include additional special characters
    single_line_file = single_line_file.strip() + " "
    
#     Flags to control the correct treatment of every character in the file
#     - expecting_ flags: the parser is waiting for that element
#     - in_ flags: the parser is currently processing that type of element
#     - no_separator: special flag to represent that the content currently being
#         read by the parser is a list of values, all to be included under the
#         same label
#     - parentheses_content: currently reading characters which will form the
#         content of a structure (both delimited by graph parentheses)
#     - parentheses_count: used to understand when an highest level set of
#         parentheses has been closed, terminating a structure (when it reaches
#         zero again after being increased)
    expecting_verb_label = True
    expecting_separator = False
    expecting_content = False
    in_verb_label = False
    in_content = False
    in_quote = False
    in_percent = False
    no_separator = False
    parentheses_content = False
    parentheses_count = 0
    
    # Last label to be read and current uninterrupted word
    last_label = ""
    current_word = ""
    
   # TODO: CHECK WEIRD PROBLEMS WITH COLONS WHEN IN CONTENT NOT ENCLOSED IN PARS
    for char in single_line_file:
        
        # print(char, in_verb_label, in_content, in_quote, '\t', expecting_verb_label, expecting_separator, expecting_content)
               
        # Special characters appearing betweem quotes are treated as raw text
        # and simply added to the current word
        if (in_quote or in_percent) and char in [' ', '{', '}', '$', '.', '\'', ':']:
            current_word += char
            continue
        
        # TODO: REMEMBER THIS CHANGE
        if in_quote and char == '%':
            current_word += char
            continue
        
        if char == " ":
            if in_verb_label:
                in_verb_label = False                
                if no_separator:
                    
                    # Without a separator, the parser is reading one element of
                    # a list of values assigned to a label, so add the word to
                    # the value list...
                    words_list.append(current_word)
                    current_word = ""
                    in_verb_label = True
                else:
                    
                    # ...otherwise start waiting for the separator, save the
                    # word as a label and put it into the values list as it
                    # could be the start of a value chain                    
                    expecting_separator = True
                    last_label = current_word
                    if words_list == []:
                        words_list.append(current_word)
                    current_word = ""                    
            elif in_content:                
                if not parentheses_content:
                    
                    # Content is concluded, start waiting for the next label
                    in_content = False
                    expecting_verb_label = True
                    verb_struct[last_label] = current_word.strip()
                    current_word = ""
                else:                    
                    current_word += char
        elif char == "{":
            
            # Since we have already dealt with the case of raw text at the
            # beginning of the loop, a parenthesis here MUST mean the start of
            # content, and must be introduced by a label and separator
            assert not expecting_verb_label
            assert not expecting_separator
            parentheses_count += 1
            if parentheses_count == 1 and expecting_content:
                expecting_content = False
                in_content = True
                parentheses_content = True
            elif in_content:    # CHANGE TO else: assert in content
                
                # Parenthesis could be part of a substructure; in that case,
                # simply add to word (this method is not recursive per se)
                current_word += char
        elif char == "}":
            parentheses_count -= 1
            assert parentheses_count >= 0
            if parentheses_count == 0:
                
                # As with the open parenthesis, a closed one must be used
                # in a content section, not by itself
                assert in_content
                in_content = False
                expecting_verb_label = True
                verb_struct[last_label] = current_word.strip()
                current_word = ""                
            else:
                
                # Substructure parenthesis
                current_word += char
        elif char == ":":
            
            # The presence of a separator means that a structure is being
            # introduced, and that the values list is not necessary
            words_list = []
            if expecting_separator:
                
                # Expected separator case (spaces between it and label)
                expecting_separator = False
                expecting_content = True
                current_word = ""
            elif in_verb_label:
                
                # Unexpected separator case (no spaces between it and label)
                in_verb_label = False
                expecting_separator = False
                expecting_content = True
                last_label = current_word
                current_word = ""                
            else:
                
                # Any other case, simply save it
                current_word += char
        elif char == '"':
            
            # Nested quotes are not accepted (use inverted commas in text),
            # since they cannot introduce a structure; they are therefore an
            # on/off symbol
            in_quote = not in_quote
            current_word += char
            
            # TODO: REMEMBER THIS CHANGE

        elif char == '%':
            in_percent = not in_percent
            current_word += char
        else:
            
            # In any other case, whatever symbol is found is either saved or
            # considered the beginning of a new element after a space
            if expecting_verb_label:
                current_word += char
                expecting_verb_label = False
                in_verb_label = True
            elif expecting_separator:
                expecting_separator = False
                no_separator = True
                in_verb_label = True
                current_word += char
            elif expecting_content:
                current_word += char
                expecting_content = False
                in_content = True
                parentheses_content = False
            else:   #elif in_verb_label or in_content:
                current_word += char
    
    # Despite the countermeasure of the artificial trailing space, there still
    # are edge cases where elements may be ignored by the parser; this section
    # is a catch-all for them
    if current_word != "" and last_label != "":
        verb_struct[last_label] = current_word
    elif current_word != "":
        words_list.append(current_word)               
    
    # If even a single element was saved in the value list, return it; otherwise
    # the file wasn't a list of values, and the dictionary should be returned
    if len(words_list) > 0:
        return words_list
    else:   
        return verb_struct


# POSSIBLY DEPRECATED
def parse_verbal_struct(verbs_struct):
    
    segmented_verb_struct = {}  
    
    for verb in verbs_struct:        
        verb_cont = verbs_struct[verb]
        segmented_verb_struct[verb] = parse_verb_language(verb_cont)    
    
    return segmented_verb_struct



# This function is used to recursively parse a LANGUAGE file, dividing it into
# its component structures.
# @param linear_file - string, the file contained in a string with no newline
#     characters
# @return - dictionary/list, the structure representing the file
def recursive_file_parsing(linear_file):
    
    # Initial division into a structure
    parsed_linear_file = parse_verb_language(linear_file)
    
    # If the structure is a dictionary, continue dividing its components,
    # otherwise it's a list, and must be returned
    if type(parsed_linear_file) == dict:
        new_struct = {}
                
        for field in parsed_linear_file:
            body = parsed_linear_file[field]
            parsed_body = recursive_file_parsing(body)
            new_struct[field] = parsed_body
        
        return new_struct
    else:
        return parsed_linear_file            
            
# This function is used to check if all required base verbal forms are included
# in every verb
# @param verbs_struct - dictionary, structure containing all verbs
# @param requirements - list, contains all of the obligatory base forms
# @return - boolean, whether the verb contains all requirements or not
def validate_single_verbs(verbs_struct, requirements):
    
    for verb in verbs_struct:
        
        # The keys of the structure of a verb is the list of base forms it
        # contains
        verb_fields = list(verbs_struct[verb].keys())
        for requirement in requirements:
            if requirement not in verb_fields:
                return False
            
    return True



# Function to modify the verbs in order to be of the current format; if the
# past_simple contains a single form, that is considered the immutable one and
# propagated on all verbal persons
# @param verbs_struct - dictionary, structure containing all verbs
def format_verbs(verbs_struct):
    
    for verb in verbs_struct:
        
        verbs_fields = verbs_struct[verb]
        
        for field in verbs_fields:
            
            if field == "past_simple":
                assert type(verbs_fields[field]) == list and (len(verbs_fields[field]) == 6 or len(verbs_fields[field]) == 1)
                
                if len(verbs_fields[field]) == 1:
                    new_past_simple = [verbs_fields[field][0] for i in range(6)]
                    verbs_struct[verb][field] = new_past_simple    
    
    return

# Function to generate the complete verbs structure
# @param verbpath - string, path to the file containing all verbs
# @param requirements - list, fields every verb must have to be acceptable
# @return - dictionary, structure containing all verbs
def generate_verbs_structure(verbpath, requirements):
    
    all_verbs = {}
    
    for verb_file in os.listdir(verbpath):
        complete_filename = os.path.join(verbpath, verb_file)
        if os.path.isdir(complete_filename):
            next_level = generate_verbs_structure(complete_filename, requirements)
            all_verbs = {**all_verbs, **next_level}
        else:
            linear_verb_file = linearize_file(complete_filename)
            verb_structure = recursive_file_parsing(linear_verb_file)
            verb_validation = validate_single_verbs(verb_structure, requirements)
            
            # The computation must fail if even a single base form is missing, to
            # prevent the error from propagating on all levels of the program
            assert verb_validation
            
            format_verbs(verb_structure)
            
            all_verbs = {**all_verbs, **verb_structure}
    
    return all_verbs
    
# Function to generate the complete tenses structure
# @param tensepath - string, path to the file containing all tenses
# @return - dictionary, structure containing all tenses
def generate_tenses_structure(tensespath):    
    
    all_tenses = {}
    
    for tense_file in os.listdir(tensespath):
        complete_filename = os.path.join(tensespath, tense_file)
        if os.path.isdir(complete_filename):
            next_level = generate_tenses_structure(complete_filename)
            all_tenses = {**all_tenses, **next_level}
        else:
            linear_tense_file = linearize_file(complete_filename)
            tenses_structure = recursive_file_parsing(linear_tense_file)
            
            all_tenses = {**all_tenses, **tenses_structure}
        
        
    return all_tenses

# Function to generate the complete components structure
# @param componentspath - string, path to the file containing all components
# @return - dictionary, structure containing all components
def generate_components_structure(componentspath):
    
    all_components = {}
    
    for component_file in os.listdir(componentspath):
        complete_filename = os.path.join(componentspath, component_file)
        if os.path.isdir(complete_filename):
            next_level = generate_components_structure(complete_filename)
            all_components = {**all_components, **next_level}
        else:
            linear_component_file = linearize_file(complete_filename)
            components_structure = recursive_file_parsing(linear_component_file)  
            
            all_components = {**all_components, **components_structure}
        
    return all_components

# Function to generate the complete segments structure
# @param mailspath - string, path to the file containing all segments
# @return - dictionary, structure containing all segments
def generate_segments_structure(mailspath):
    
    all_segments = {}
    
    for segment_file in os.listdir(mailspath):
        complete_filename = os.path.join(mailspath, segment_file)
        if os.path.isdir(complete_filename):
            next_level = generate_segments_structure(complete_filename)
            all_segments = {**all_segments, **next_level}
        else:
            linear_segment_file = linearize_file(complete_filename)
            segments_structure = recursive_file_parsing(linear_segment_file)
            
            all_segments = {**all_segments, **segments_structure}
     
    
    return all_segments


# Function to generate the complete actions structure
# @param actionspath - string, path to the file containing all actions
# @return - dictionary, structure containing all actions
def generate_actions_structure(actionspath):
    
    all_actions = {}
    
    for action_file in os.listdir(actionspath):
        complete_filename = os.path.join(actionspath, action_file)
        if os.path.isdir(complete_filename):
            next_level = generate_actions_structure(complete_filename)
            all_actions = {**all_actions, **next_level}
        else:
            linear_action_file = linearize_file(complete_filename)
            actions_structure = recursive_file_parsing(linear_action_file)
            
            # Users could define complements which are supposed to be introduced by
            # no prepositions as 'complement : {}'. Since this would cause the
            # parentheses to be interpreted as an empty structure instead of an empty
            # list, and cause a runtime failure, empty parentheses have to be actively
            # replaced with list containing the empty string.
            # Note that 'complement : {""}' is a valid spelling.
            for action in actions_structure:
                action_body = actions_structure[action]
                for verb in action_body:
                    verb_body = action_body[verb]
                    verb_logics = verb_body["logics"]
                    for complement in verb_logics:
                        if verb_logics[complement] == {}:
                            verb_logics[complement] = [""]
            
            all_actions = {**all_actions, **actions_structure}       
    
    return all_actions

# Function to generate the structure cataloguing all of the categories and their
# elements
# @param catpath - string, path to the directory containing all categories
# @return - dictionary, structure containing all categories
def generate_categories_structure(catpath):
    
    categories_struct = {}
    
    # List all of the category files
    categories = os.listdir(catpath)
    
    # Category elements are arrayed in a format of one per line
    for category in categories:
        complete_filename = os.path.join(catpath, category)
        if os.path.isdir(complete_filename):
            next_level = generate_categories_structure(complete_filename)
            categories_struct = {**categories_struct, **next_level}
        else:
            category_file = open(complete_filename, 'r')
            category_elements = category_file.readlines()
            category_file.close()
            
            assert category.count('.') == 1
                    
            category_name = category.split('.')[0]
            categories_struct[category_name] = {}
            
            # Save every element in the structure under the appropriate category
            for category_element in category_elements:
                clean_category_element = str(category_element).strip()
                if clean_category_element != '':
                    category_element_list = clean_category_element.split(":::")
                    category_element_name = category_element_list[0]
                    category_element_body = category_element_list[1:]
                    
                    categories_struct[category_name][category_element_name] = category_element_body

    
    return categories_struct

# Function to generate the structure cataloguing all of the special
# symbols
# @param sympath - string, path to the directory containing all symbol files
# @return - dictionary, structure containing all symbols
def generate_special_symbols_structure(sympath):
    
    all_symbols = {}
    
    for symbol_file in os.listdir(sympath):
        complete_filename = os.path.join(sympath, symbol_file)
        if os.path.isdir(complete_filename):
            next_level = generate_special_symbols_structure(complete_filename)
            all_symbols = {**all_symbols, **next_level}
        else:
            linear_symbol_file = linearize_file(complete_filename)
            symbol_structure = recursive_file_parsing(linear_symbol_file)
            
            all_symbols = {**all_symbols, **symbol_structure}
     
    
    return all_symbols
