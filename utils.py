# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 16:45:30 2020

@author: Ale
"""

import random

# Extract the value associated with the n-th key. While dictionaries dictionaries
# are unordered, this is useful in some occasions, like extracting the first
# and only key in a dictionary of size 1.
# @param d - dicitonary, the dictionary to extract the key from
# @param n - integer, the position of the key to access
# @return - the value associated to the n-th key
def nthkey(d, n=0):

    assert type(d) == dict
    assert len(list(d.keys())) > n

    return list(d.keys())[n]

# Extract a random element from a dictionary
# @param d - dicitonary, the dicitonary to extract the random value from
# @return - the value associated to the randomly selected key
def randkey(d):

    assert len(d) > 0

    return list(d.keys())[random.randint(0, len(d)-1)]



def collection_copy(col):
    
    if type(col) == dict:
        copy = {}
        for c in col:
            ret = collection_copy(col[c])
            copy[c] = ret
        return copy
    
    elif type(col) == list:
        copy = []
        for c in col:
            ret = collection_copy(c)
            copy.append(ret)
        return copy
    
    else:
        return col